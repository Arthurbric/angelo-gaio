package main

import (
	"fmt"
	"math"
)

func main() {
	var A, B, C float64

	fmt.Print("Digite o valor de A: ")
	fmt.Scan(&A)
	fmt.Print("Digite o valor de B: ")
	fmt.Scan(&B)
	fmt.Print("Digite o valor de C: ")
	fmt.Scan(&C)
	println()
	fmt.Printf("Área do triângulo: %.2f\n", (A*C)/2)
	fmt.Printf("Área do círculo: %.2f\n", math.Pi*math.Pow(C, 2))
	fmt.Printf("Área do trapézio: %.2f\n", ((A+B)*C)/2)
	fmt.Printf("Área do quadrado: %.2f\n", math.Pow(B, 2))
	fmt.Printf("Área do retângulo: %.2f\n", A*B)
}
