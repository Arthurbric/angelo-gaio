package main

import "fmt"

func main() {
	var x, y float32

	fmt.Println("Escolha dois valores (x y): ")
	fmt.Scan(&x, &y)

	if x == 0 && y == 0 {
		fmt.Print("Origem")
	} else {
		posicao(x, y)
	}
}

func posicao(x, y float32) {
	if x == 0 && y != 0 {
		fmt.Println("Eixo Y")
	} else if x != 0 && y == 0 {
		fmt.Println("Eixo X")
	} else if x > 0 && y > 0 {
		fmt.Println("Superior Direito")
	} else if x < 0 && y > 0 {
		fmt.Println("Superior Esquerdo")
	} else if x < 0 && y < 0 {
		fmt.Println("Inferior Esquerdo")
	} else {
		fmt.Println("Inferior Direito")
	}
}
